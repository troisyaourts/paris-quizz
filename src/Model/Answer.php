<?php

namespace App\Model;

use Alpha\Entity;
use Alpha\Manager;
use Doctrine\DBAL\Query\QueryBuilder;

class Answer extends Entity
{
  const TABLE_NAME = 'pquizz_answer';


  static public function getByQuestionId(Manager $manager, $question_id){
    $q = $manager->query()
                    ->andWhere('question_id = :question_id')
                    ->setParameter("question_id", $question_id)
                    ->addOrderBy("display_order", "ASC")
                    ;
    return $manager->fetchAll($q);
  }

  static public function incrementStat(Manager $manager, $answer_id){

    $qb = new QueryBuilder($manager->db);
    $q = $qb->update(static::TABLE_NAME, "a")
      ->set("a.stats", "a.stats + 1")
      ->where("id = :answer_id")
      ->setParameter("answer_id", $answer_id);
  //  var_dump($q->__toString());
  //  die();
    return $q->execute();
  }
}
