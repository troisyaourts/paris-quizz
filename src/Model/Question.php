<?php

namespace App\Model;

use Alpha\Entity;
use Alpha\Manager;

class Question extends Entity
{
  const TABLE_NAME = 'pquizz_question';

  public function getAnswers(){
    $nomManager = $this->manager->related("Answer");
    $q = $nomManager->query()
                    ->andWhere('question_id = :question_id')
                    ->setParameter("question_id", $this->id)
                    ->addOrderBy("display_order", "ASC")
                    ;
    return $nomManager->fetchAll($q);
  }

/*
  public function getFirst(){
    $q = $this->manager->query()
              ->orderBy("display_order ASC")
              ;
    return $this->manager->fetchOne($q);
  }
*/
  public function getPrevious(){
    $q = $this->manager->query()
              ->andWhere('display_order < :current_order')
              ->setParameter("current_order", $this->display_order)
              ->orderBy("display_order",  "DESC")
              ;
    return $this->manager->fetchOne($q);
  }

  public function getNext(){
    $q = $this->manager->query()
              ->andWhere('display_order > :current_order')
              ->setParameter("current_order", $this->display_order)
              ->orderBy("display_order","ASC")
              ;
    return $this->manager->fetchOne($q);
  }
}
