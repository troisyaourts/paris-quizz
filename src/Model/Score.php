<?php

namespace App\Model;

use Alpha\Entity;
use Alpha\Manager;

class Score extends Entity
{
  const TABLE_NAME = 'pquizz_score';


  static public function getByValue(Manager $manager, $score){
    $q = $manager->query()
                    ->andWhere('value = :score')
                    ->setParameter("score", $score)
                    ;
    return $manager->fetchOne($q);
  }

}
