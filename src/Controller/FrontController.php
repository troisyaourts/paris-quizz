<?php
namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Twig\Environment;
use Alpha\ModelProvider;

class FrontController extends AbstractController
{
    private $twig;
    private $models;

    public function __construct(Environment $twig, ModelProvider $models)
    {
        $this->twig = $twig;
        $this->models = $models;
    }


    public function index(Request $request): Response
    {

        $score = $request->query->get("s");
        $total = $request->query->get("t");

        return $this->render('index.html.twig', [
          "score" => $score,
          "total" => $total
        ]);
    }
}
