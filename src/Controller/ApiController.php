<?php
namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Alpha\ModelProvider;
use App\Services\UUID;

class ApiController extends AbstractController
{

    private $models;

    public function __construct(ModelProvider $models)
    {
        $this->models = $models;
    }

    public function questions(Request $request): JsonResponse
    {
        $qManager = ($this->models)("Question");
        $q = $qManager->query()->orderBy('display_order', 'ASC');
        $questions = $qManager->fetchAll($q);
        return new JsonResponse(["questions" => $questions->dumpPile()]);
    }

    public function question(Request $request, string $question): JsonResponse
    {
        $qManager = ($this->models)("Question");
        $q = $qManager->query()->orderBy('display_order', 'ASC');
        $questions = $qManager->fetchAll($q);
        $question = $qManager->get($question);
        $answers = $question->getAnswers();
        $previous = $question->getPrevious();
        $next     = $question->getNext();
        return new JsonResponse([
                                 "allQuestions" => $questions->dumpPile(),
                                 "question" => $question,
                                 "answers" => $answers->dumpPile(),
                                 "previous" => $previous,
                                 "next"     => $next,
                                ]);
    }

    public function answer(Request $request){
        $content = $request->getContent();
        $data = json_decode($content, true);

        $question_id = $data["question_id"];
        $answer_id = $data["answer_id"];
        ($this->models)("Answer")->incrementStat($answer_id);
        $answers = ($this->models)("Answer")->getByQuestionId($question_id);
        return new JsonResponse(["status" => "ok", "message" => "vote enregistré", "answers" => $answers->dumpPile()]);
    }

    public function saveScore(Request $request){
      $content = $request->getContent();
      $data = json_decode($content, true);
      $score = ($this->models)("Score")->getByValue($data["score"]);
      if($score->id == 0){
        $score->value = $data["score"];
        $score->stats = 1;
      }else{
        $score->stats++;
      }
      $score->save();
      return new JsonResponse(["status" => "ok", "message" => "score enregistré"]);
    }

    public function getRank(Request $request){
      $sManager = ($this->models)("Score");
      $q = $sManager->query()->orderBy('value', 'ASC');
      $scores = $sManager->fetchAll($q);
      $total = 0;
      $below = 0;
      foreach($scores as $score){
        $total += $score->stats;
        if($score->value < $request->query->get("score")) $below += $score->stats;
      }
      return new JsonResponse(["total" => $total, "below" => $below]);
    }
}
