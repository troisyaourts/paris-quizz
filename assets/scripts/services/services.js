'use strict';


app.factory('answerservice', ['$http', function($http){
  var service = this;
  service.currentAnswers = {};

  return {
          registerAnswer: registerAnswer,
          saveAnswer: saveAnswer,
          reset: reset,
          getAnswer: getAnswer,
          computeScore: computeScore
        };

  function registerAnswer(question_id, answer_id, is_correct){
    service.currentAnswers[question_id] = {
      "answer_id": answer_id,
      "is_correct" : is_correct
    };
  }

  function getAnswer(question_id){
    return service.currentAnswers[question_id]
  }

  function reset(){
    service.currentAnswers = {};
  }


  function saveAnswer(question_id, answer_id) {
    var res = $http({url: '/api/answer',
        method: 'POST',
        data: {
          'question_id': question_id,
          'answer_id': answer_id
        }
      })
      .then(saveAnswerComplete)
      .catch(saveAnswerFailed);

    return res;

    function saveAnswerComplete(response) {
        return response.data.answers;
    }

    function saveAnswerFailed(error) {
        console.error('XHR Failed for saveAnswer.');
        throw error;
    }
  }

  function computeScore(){
    var correct = 0;
    var total = 0;
    for(var question_id in service.currentAnswers){
      var answer = service.currentAnswers[question_id];
      total++;
      if(answer.is_correct) correct++;
    }
    return {
      correct: correct,
      total: total
    }
  }

}]);

app.factory('questionservice', ['$http', function($http){
  return {
    getQuestions: getQuestions,
    getAnswersForQuestion: getAnswersForQuestion,
  };

  function getQuestions() {
    return $http.get('/api/questions')
        .then(getQuestionsComplete)
        .catch(getQuestionsFailed);

    function getQuestionsComplete(response) {
        return response.data.questions;
    }

    function getQuestionsFailed(error) {
        console.error(error.data);
        return error.data;
    }
  }

  function getAnswersForQuestion(question) {
    return $http({url: '/api/question/'+question,
                  method: 'GET'
                })
        .then(getAnswersForQuestionComplete)
        .catch(getAnswersForQuestionFailed);

    function getAnswersForQuestionComplete(response) {
      console.log("getAnswersForQuestionComplete", response.data);
        return response.data;
    }

    function getAnswersForQuestionFailed(error) {
        console.error('XHR Failed for getAnswersForQuestion.');
        return error.data;
    }
  }
}]);

app.factory('scoreservice', ['$http', function($http){
  return {
    saveScore: saveScore,
    getRank: getRank
  }

  function saveScore(score) {
    return $http({url: '/api/save-score',
        method: 'POST',
        data: {
          'score': score
        }
      })
        .then(saveScoreComplete)
        .catch(saveScoreFailed);

    function saveScoreComplete(response) {
        return response.data;
    }

    function saveScoreFailed(error) {
        console.error(error.data);
        return error.data;
    }
  }

  function getRank(score) {
    console.log("rang");
    return $http.get('/api/rank?score='+ score)
        .then(getRankComplete)
        .catch(getRankFailed);

    function getRankComplete(response) {
      console.log("getRank ", response.data);
        return response.data;
    }

    function getRankFailed(error) {
        console.error(error.data);
        return error.data;
    }
  }
}]);
