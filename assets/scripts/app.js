'use strict';

var app = angular.module("parisquizz", ['ngRoute', 'ngCookies']);

app.config(["$interpolateProvider", function($interpolateProvider) {
    $interpolateProvider.startSymbol('{$');
    $interpolateProvider.endSymbol('$}');
}]);

// app.config(function($mdThemingProvider) {
//     $mdThemingProvider.theme('docs-dark', 'default')
//       .primaryPalette('teal')
//       .dark();
// });


app.config(["$routeProvider", "$locationProvider", function($routeProvider, $locationProvider) {
  $routeProvider
   .when('/', {
    templateUrl: 'templates/home.html',
    controller: 'HomeController',
    controllerAs: 'ctrl'
  })
  .when('/question/:question', {
    templateUrl: 'templates/question.html',
    controller: 'QuestionController',
    controllerAs: 'ctrl'
  })
  .when('/end', {
    templateUrl: 'templates/end.html',
    controller: 'EndController',
    controllerAs: 'ctrl'
  })
  ;

  // configure html5 to get links working on jsfiddle
  $locationProvider.html5Mode(true);
}]);

app.run(['$rootScope', '$window',
  function($rootScope, $window) {

/*
  $rootScope.user = {};
  $window.fbAsyncInit = function() {
    // Executed when the SDK is loaded
    FB.init({
      appId: 147410975925814,
      channelUrl: 'app/channel.html',
      status: true,
      cookie: true,
      xfbml: true,
      version: 'v2.4'
    });
    FB.Canvas.setAutoGrow();
  };

  (function(d){
    // load the Facebook javascript SDK

    var js,
    id = 'facebook-jssdk',
    ref = d.getElementsByTagName('script')[0];

    if (d.getElementById(id)) {
      return;
    }

    js = d.createElement('script');
    js.id = id;
    js.async = true;
    js.src = "https://connect.facebook.net/en_US/sdk.js";

    ref.parentNode.insertBefore(js, ref);

  }(document));*/

}]);
