'use strict';


app.controller("HomeController",
  ['questionservice', '$location', '$window', '$cookies',
    function(questionservice, $location, $window, $cookies){

      questionservice.getQuestions().then(function(questions){
        console.log(questions);
        ctrl.first = questions[0];
      })

      var ctrl = this;

      ctrl.enter = function(){
        $location.path("/question/"+ ctrl.first.id);
      }




      ctrl.cookiesAccepted = $cookies.get("cookies-accepted");
      ctrl.acceptCookies = function(){
        ctrl.cookiesAccepted = true;
        $cookies.put("cookies-accepted", true);
      }


  }]);

app.controller("QuestionController",
  ['questionservice', 'answerservice', 'scoreservice', '$routeParams', '$location', '$window', '$timeout',
        function(questionservice, answerservice, scoreservice, $routeParams, $location, $window, $timeout){
  var ctrl = this;
  var question_id = $routeParams.question;
  var question = {};
  var answers = [];
  var previous = {};
  var next = {};

  var questions_promise = questionservice.getAnswersForQuestion(question_id);

  var url = $window.location.href;
  var arr = url.split("/");
  ctrl.appLink =   arr[0] + "//" + arr[2];
  ctrl.appUrl =   arr[2];

  ctrl.isLoading = true;
  ctrl.isAnswered = (answerservice.getAnswer(question_id) !== undefined);

  questions_promise.then(function(data){
    ctrl.previous = data.previous;
    ctrl.question = data.question;
    ctrl.answers = data.answers;
    ctrl.allQuestions = data.allQuestions;

    ctrl.numQuestions = ctrl.allQuestions.length;
    ctrl.questionRank = 0;

    ctrl.questionsTrack = [];

    ctrl.previousQuestion = null;
    ctrl.nextQuestion = null;

    var current_passed = false;
    for(var i=0; i< ctrl.allQuestions.length; i++){
      if(!current_passed) ctrl.questionRank ++;
      var q = ctrl.allQuestions[i];
      var answered = (answerservice.getAnswer(q.id) !== undefined);
      if(current_passed && ctrl.nextQuestion ==null && answered) ctrl.nextQuestion = q.id;
      if(q.id == question_id) current_passed = true;
      if(!current_passed) ctrl.previousQuestion = q.id;
      ctrl.questionsTrack.push({
        accessible: (answered || q.id == question_id) ,
        id:  q.id,
        current: (q.id == question_id)
      })
    }


    // redirect to to first non answerd question if necessary
    if(!ctrl.isAnswered){
      var first_non_answered = null;
      for(var i=0; i< ctrl.allQuestions.length; i++){
        var q = ctrl.allQuestions[i];
        if(!answerservice.getAnswer(q.id)){
          first_non_answered = q.id;
          break;
        }
      }
      if(first_non_answered != null && first_non_answered != question_id){
        $location.path("/question/"+first_non_answered);
      }
    }else{

    }

    computeStats();

    ctrl.next = data.next;
    ctrl.isLoading = false;

    animateStats();

  });


function animateStats(){
  $timeout(function(){
    var elts = document.querySelectorAll(".stats-bg-init")
    for(var i=0; i< elts.length; i++){
      var e = elts[i];
      e.className = e.className.replace("stats-bg-init", "");

    }
  }, 1600)
}

  function computeStats(){
    var total = 0;
    ctrl.answers.forEach(function(e){
      total += +e.stats;
    });
    ctrl.answers.forEach(function(e){
      e.statsPercent = (100 * e.stats / total).toFixed(0);
    });
  }


  ctrl.choice = null;
  if(answerservice.getAnswer(question_id)){
    ctrl.choice = answerservice.getAnswer(question_id).answer_id;
  }

  ctrl.gotoNext = function(){
    $window.scrollTo(0, 0);
    if(typeof(ctrl.next.id) != "undefined"){
      $location.path("/question/"+ctrl.next.id);
    }else{
      var score = answerservice.computeScore().correct;
      scoreservice.saveScore(score).then(function(data){
        console.log(data);
        $location.path("/end");
      })

    }
  }

  ctrl.go = function(id){
    $location.path("/question/"+id);
  }


  ctrl.refresh = function(){
    ctrl.isLoading = true;
    ctrl.isAnswered = true;

    questionservice.getAnswersForQuestion(question_id).then(function(data){
      ctrl.answers = data.answers;
      computeStats();
      ctrl.isLoading = false;
      animateStats();
    });
  }


  ctrl.answer = function(){
    ctrl.isLoading = true;
    console.log("answered "+ ctrl.choice+ " on " + question_id);
    ctrl.isAnswered = true;
    answerservice.saveAnswer(question_id, ctrl.choice).then(function(updatedAnswers){
        ctrl.answers = updatedAnswers;
        computeStats();
        ctrl.isLoading = false;
        animateStats();
    });
    //ctrl.gotoNext();

  }

  ctrl.choose = function(answer_id){
    if(ctrl.isAnswered) return;
    console.log("chose "+ answer_id+ " on " + question_id);
    var is_correct = false;
    ctrl.answers.forEach(function(e){ console.log("e", e);
      if(e.id == answer_id && e.is_correct == "1"){
        is_correct = true;
      }
    })

    answerservice.registerAnswer(question_id, answer_id, is_correct);
    ctrl.choice = answer_id
  }

  ctrl.openMentions = function(){
    angular.element(document.body).addClass("mentions-is-open")
  };
  ctrl.closeMentions = function(){
    angular.element(document.body).removeClass("mentions-is-open")
  };

}]);





app.controller("EndController",
  [ 'questionservice', 'answerservice',  'scoreservice', '$location', '$timeout', '$window', function(questionservice, answerservice, scoreservice,  $location, $timeout, $window){
  var ctrl = this;

  ctrl.saveError = false;
  ctrl.loading   = true;

  ctrl.score = answerservice.computeScore();
  ctrl.rank = "";

  var url = window.location.href;
  var arr = url.split("/");
  ctrl.appLink =   encodeURIComponent(arr[0] + "//" + arr[2] + "/");

  // redirect if non answered question
  questionservice.getQuestions().then(function(questions){
    console.log("questions", questions);
      var first_non_answered = null;
      for(var i=0; i< questions.length; i++){
        var q = questions[i];
        if(!answerservice.getAnswer(q.id)){
          first_non_answered = q.id;
          break;
        }
      }
      if(first_non_answered != null){
        $location.path("/question/"+first_non_answered);
      }
      scoreservice.getRank(ctrl.score.correct).then(function(ranking){
        ctrl.rank = (100 * ranking.below / ranking.total).toFixed(0);
      //  ctrl.rank = 77;

        ctrl.loading = false;
        animateMark();
      })
  });

  function animateMark(){
    $timeout(function(){
      var elts = document.querySelectorAll(".score-meter-mark-init");
      for(var i=0; i< elts.length; i++){
        var e = elts[i];
        e.className = e.className.replace("score-meter-mark-init", "");
      }
    }, 22)
  }


  ctrl.facebook = function(){
    var url = "https://www.facebook.com/sharer/sharer.php?u=" + ctrl.appLink +"%3Fs=" +  ctrl.score.correct +"%26t=" + ctrl.score.total;
    $window.open(url, "facebook", "height=600, width=600" );
  }
  ctrl.twitter = function(){
    var url = "https://twitter.com/home?status=Passez%20le%20quizz%20du%20budget%20Paris%202018%3A%20" + ctrl.appLink +"%20J'ai%20obtenu%20" +  ctrl.score.correct + "/" + ctrl.score.total + "%0A";
    $window.open(url, "twitter", "height=600, width=600" );
  }


  ctrl.restart = function(){
    answerservice.reset();
    $location.path("/");
  }

  ctrl.share = function(){
    FB.ui({ method: 'share', href: 'https://sfrplayawards.fr', }, function(response){});
  }
}]);
