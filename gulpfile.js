var gulp = require('gulp');
var $    = require('gulp-load-plugins')();
var merge = require('merge-stream');

/***
 *  CHOOSE HERE WHAT YOU WANT
 ***/

 var app_name = "paris-quizz";

var frontSassPaths = [
  'bower_components/foundation-sites/scss/',
  'bower_components/motion-ui/src',
];

var frontCssPaths = [
];

var frontJsPaths = [
    'bower_components/angular/angular.js',
    'bower_components/angular-route/angular-route.js',
    'bower_components/angular-cookies/angular-cookies.js',
    'bower_components/angular-animate/angular-animate.js',
    'assets/scripts/app.js',
    'assets/scripts/**/*.js',
];



gulp.task('images', function(){
  gulp.src('assets/images/**/*')
    .pipe($.cache($.imagemin({ optimizationLevel: 3, progressive: true, interlaced: false })))
    .pipe(gulp.dest('public/dist/img/'));
});

gulp.task('icons', function() {
    return gulp.src([
      'bower_components/font-awesome/fonts/**.*'
      ])
        .pipe(gulp.dest('./public/dist/fonts'));
});



function compute_style(output, sassEntryPoint, sassPaths, cssPaths){
  var sassStream = gulp.src(sassEntryPoint)
                    .pipe($.sass({
                        includePaths: sassPaths,
                        outputStyle: 'compressed'
                  }).on('error', $.sass.logError));
  var cssStream  = gulp.src(cssPaths);
  return merge(cssStream, sassStream)
    .pipe($.concat(output+'.css'))
    .pipe($.autoprefixer({
      browsers: ['last 2 versions', 'ie >= 9']
    }))
    .pipe($.cssnano())
    .pipe(gulp.dest('./public/dist/css'))
    //minify ?
}

function compute_script(output, jsPaths){
  return gulp.src(jsPaths)
  .pipe($.plumber({
      errorHandler: function (error) {
        console.log(error.message);
        this.emit('end');
    }}))
    .pipe($.concat(output+'.js'))
    .pipe(gulp.dest('./public/dist/js'))
    .pipe($.rename({suffix: '.min'}))
    .pipe($.uglify())
    .pipe(gulp.dest('./public/dist/js'))
}

gulp.task('styles', function(){
  compute_style(app_name, 'assets/scss/app.scss', frontSassPaths, frontCssPaths);
});

gulp.task('scripts', function(){
  compute_script(app_name, frontJsPaths);
});

gulp.task('default', ['styles', 'scripts'], function(){
  gulp.watch("assets/scss/*.scss", ['styles']);
  gulp.watch("assets/scripts/**/*.js", ['scripts']);
});
