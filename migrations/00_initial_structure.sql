CREATE TABLE `pquizz_answer` (
  `id` int(11) NOT NULL,
  `question_id` int(11) NOT NULL,
  `label` varchar(255) NOT NULL,
  `is_correct` tinyint(1) NOT NULL,
  `stats` int(10) UNSIGNED NOT NULL,
  `display_order` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

ALTER TABLE `pquizz_answer`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `pquizz_answer`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;




CREATE TABLE `pquizz_question` (
  `id` int(11) NOT NULL,
  `question` text NOT NULL,
  `display_order` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

ALTER TABLE `pquizz_question`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `pquizz_question`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;



CREATE TABLE `pquizz_score` (
  `id` int(11) NOT NULL,
  `value` int(10) UNSIGNED NOT NULL,
  `stats` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

ALTER TABLE `pquizz_score`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `pquizz_score`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
